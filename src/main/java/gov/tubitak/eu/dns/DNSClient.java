package gov.tubitak.eu.dns;

import org.xbill.DNS.*;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;

public class DNSClient {

  private static final int DEFAULT_PORT = 9988;

  public String[] issueDNSQuery(String host,int port, String query, int type, int dclass, boolean recursion) throws TextParseException, UnknownHostException{

    Lookup lookup = new Lookup(query,type,dclass);
    Resolver resolver = new SimpleResolver(host);

    resolver.setPort(port);

    lookup.setResolver(resolver);
    lookup.setCache(null);

    Record rrecords[] = lookup.run();

    ArrayList<String> response = new ArrayList<String>();
    String rrResponseDomain;

    if(lookup.getResult() == Lookup.SUCCESSFUL){

      for(Record rec : rrecords){
        //System.out.println(rec.toString());

        if(rec instanceof PTRRecord) {
          ((PTRRecord) rec).getTarget().toString();
          rrResponseDomain =  ((PTRRecord) rec).getTarget().toString();
        }
        else if(rec instanceof URIRecord){
          rrResponseDomain = ((URIRecord) rec).getTarget().toString();
        }
        else{
          rrResponseDomain = rec.toString();
        }
        response.add(rrResponseDomain); //rec.toString()
      }
    }

    return response.toArray(new String[0]);


  }


}
