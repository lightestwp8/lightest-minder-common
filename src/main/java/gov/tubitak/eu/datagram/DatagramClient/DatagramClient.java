package gov.tubitak.eu.datagram.DatagramClient;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.net.SocketException;
import java.util.Arrays;

public class DatagramClient {

  /**
   * A static buffer that serve as the buffer for receiving the data from the other side during the {@link #sendAndReceiveOnePacket}
   * operation
   */
  private static final byte[] tmpBuffer = new byte[1024];

  /**
   * The timeout to wait for the <code>receive</code> operation on the datagram socket during {@link #sendAndReceiveOnePacket} operation
   */
  private static int responseTimeout = 10000;

  /**
   * Sends the data in the specificied buffer <code>reqBuffer</code> to the UDP server that listens on <code>host:port</code>.
   */
  public static byte[] sendAndReceiveOnePacket(byte[] reqBuffer, String host, int port) throws SocketException {
    if (reqBuffer == null || reqBuffer.length == 0) {
      throw new IllegalArgumentException("reqBuffer must contain data");
    }

    if (host == null) {
      throw new NullPointerException("Host name is null");
    }

    //send datagramPacket to DNS Server
    DatagramPacket prPacket = new DatagramPacket(reqBuffer, reqBuffer.length);
    DatagramSocket proxyClient = new DatagramSocket();
    proxyClient.connect(new InetSocketAddress(host, port));
    try {
      proxyClient.send(prPacket);

      // receive DNS Response
      //make sure that only one thread can use the tmp buffer
      //this is a temporary solution. we can later use 'ThreadLocal' in order
      //to ensure thread safety while not performing unnecessary allocations
      synchronized (tmpBuffer) {
        DatagramPacket respPacket = new DatagramPacket(tmpBuffer, tmpBuffer.length);
        proxyClient.setSoTimeout(responseTimeout);
        proxyClient.receive(respPacket);

        if (respPacket.getLength() <= 0) {
          throw new IllegalStateException("Couldn't read from the UDP server");
        }

        //return the actually read data (not all the buffer)
        return Arrays.copyOf(tmpBuffer, respPacket.getLength());
      }

    } catch (IOException e) {
      throw new IllegalStateException(e.getMessage(), e);
    }
  }


  public static void setResponseTimeout(int responseTimeout) {
    DatagramClient.responseTimeout = responseTimeout;
  }
}
