package gov.tubitak.eu.datagram.DatagramParser;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.CharBuffer;
import java.util.Arrays;
import java.util.Objects;

public class DNSDatagramMessage {

  private static final short RESPONSE_MASK = (short) 0x8000;
  private static final short OPCODE_MASK = (short) 0x7800;
  private static final int OPCODE_SHIFT = 11;
  private static final short AA_MASK = (short) 0x0400;
  private static final short TC_MASK = (short) 0x0200;
  private static final short RD_MASK = (short) 0x0100;
  private static final short RA_MASK = (short) 0x0080;
  private static final short RCODE_MASK = (short) 0x000f;

  private final short id;
  private final short flags;
  private final DNSQuery[] queries;
  private final DNSResourceRecord[] answers;
  private final DNSResourceRecord[] nameservers;
  private final DNSResourceRecord[] additionalrecords;

  public DNSDatagramMessage(short id, short flags, short queryNo, short answerNo, short nameserversNo, short additionalrecordsNo) {
    this.id = id;
    this.flags = flags;
    this.queries = new DNSQuery[queryNo];
    this.answers = new DNSResourceRecord[answerNo];
    this.nameservers = new DNSResourceRecord[nameserversNo];
    this.additionalrecords = new DNSResourceRecord[additionalrecordsNo];
  }

  public short getId() {
    return id;
  }

  public short getFlags() {
    return flags;
  }

  public DNSQuery[] getQueries() {
    return queries;
  }

  public DNSResourceRecord[] getAnswers() {
    return answers;
  }

  public DNSResourceRecord[] getNameservers() {
    return nameservers;
  }

  public DNSResourceRecord[] getAdditionalrecords() {
    return additionalrecords;
  }

  public final boolean isResponse() {
    return (flags & RESPONSE_MASK) != 0;
  }

  public final boolean isAuthoritativeAnswer() {
    return (flags & AA_MASK) != 0;
  }

  public final boolean isTruncated() {
    return (flags & TC_MASK) != 0;
  }

  public final boolean isRecursionDesired() {
    return (flags & RD_MASK) != 0;
  }

  public final boolean isRecursionAvailable() {
    return (flags & RA_MASK) != 0;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (!(o instanceof DNSDatagramMessage)) return false;
    DNSDatagramMessage that = (DNSDatagramMessage) o;
    return getId() == that.getId() &&
        getFlags() == that.getFlags() &&
        Arrays.equals(getQueries(), that.getQueries()) &&
        Arrays.equals(getAnswers(), that.getAnswers()) &&
        Arrays.equals(getNameservers(), that.getNameservers()) &&
        Arrays.equals(getAdditionalrecords(), that.getAdditionalrecords());
  }

  @Override
  public int hashCode() {

    int result = Objects.hash(getId(), getFlags());
    result = 31 * result + Arrays.hashCode(getQueries());
    result = 31 * result + Arrays.hashCode(getAnswers());
    result = 31 * result + Arrays.hashCode(getNameservers());
    result = 31 * result + Arrays.hashCode(getAdditionalrecords());
    return result;
  }

  @Override
  public String toString() {
    return "DNSDatagramMessage{" +
        "id=" + id +
        ", flags=" + flags +
        ", queries=" + Arrays.toString(queries) +
        ", answers=" + Arrays.toString(answers) +
        ", nameservers=" + Arrays.toString(nameservers) +
        ", additionalrecords=" + Arrays.toString(additionalrecords) +
        '}';
  }

  public static final String parseName(ByteBuffer buffer)
      throws Exception {
    int jumps = 0;
    int lastpos = -1;
    CharBuffer dst = CharBuffer.allocate(255);
    while (true) {
      final byte b = buffer.get();
      if (b == 0)
        break;
      switch (b & 0xC0) {
        case 0x00:
          // length that follows
          if (dst.position() != 0) {
            if (dst.remaining() < b + 1)
              throw new Exception("DNS name too long");
            dst.put('.');
          } else {
            if (dst.remaining() < b)
              throw new Exception("DNS name too long");
          }
          for (int i = 0; i < b; ++i) {
            dst.put((char) (buffer.get() & 0xff));
          }
          break;
        case 0xc0:
          // offset of the new position
          if (++jumps >= 16)
            throw new Exception("Too many DNS name jumps");
          final int offset = ((b & 0x3f) << 8) | (buffer.get() & 0xff);
          if (lastpos == -1)
            lastpos = buffer.position();
          buffer.position(offset);
          break;
        default:
          throw new Exception("Unsupported DNS name byte");
      }
    }
    if (lastpos != -1)
      buffer.position(lastpos);
    return dst.flip().toString();
  }

  public static final DNSQuery parseQuestion(ByteBuffer buffer)
      throws Exception {
    final String name = parseName(buffer);
    final short qtype = buffer.getShort();
    final short qclass = buffer.getShort();
    return new DNSQuery(name, qtype, qclass);
  }

  public static final DNSResourceRecord parseResourceRecord(ByteBuffer buffer)
      throws Exception {
    final String name = parseName(buffer);
    final short rtype = buffer.getShort();
    final short rclass = buffer.getShort();
    final int ttl = buffer.getInt();
    final int rdlength = buffer.getShort() & 0xffff;
    final byte[] rdata = new byte[rdlength];
    buffer.get(rdata);
    return new DNSResourceRecord(name, rtype, rclass, ttl, rdata);
  }

  public static final DNSDatagramMessage parse(ByteBuffer buffer)
      throws Exception {
    return parse(buffer, true);
  }

  public static final DNSDatagramMessage parse(ByteBuffer buffer, boolean complete)
      throws Exception {
    assert buffer.order() == ByteOrder.BIG_ENDIAN;
    final short id = buffer.getShort();
    final short flags = buffer.getShort();
    final short qdcount = buffer.getShort();
    final short ancount = buffer.getShort();
    final short nscount = buffer.getShort();
    final short arcount = buffer.getShort();
    if (qdcount < 0 || qdcount > 256)
      throw new Exception("Too many questions");
    if (ancount < 0 || ancount > 256)
      throw new Exception("Too many answers");
    if (nscount < 0 || nscount > 256)
      throw new Exception("Too many nameserver records");
    if (arcount < 0 || arcount > 256)
      throw new Exception("Too many additional records");
    final DNSDatagramMessage msg = new DNSDatagramMessage(id, flags, qdcount,
        complete ? ancount : 0, complete ? nscount : 0,
        complete ? arcount : 0);
    for (int i = 0; i < qdcount; ++i) {
      msg.queries[i] = parseQuestion(buffer);
    }
    if (msg.isResponse()) {
      try {
        for (int i = 0; i < ancount; ++i) {
          msg.answers[i] = parseResourceRecord(buffer);
        }
        for (int i = 0; i < nscount; ++i) {
          msg.nameservers[i] = parseResourceRecord(buffer);
        }
        for (int i = 0; i < arcount; ++i) {
          msg.additionalrecords[i] = parseResourceRecord(buffer);
        }
      } catch (Exception e) {
        // Failure to read answers is not fatal in our case
      }
    }
    return msg;
  }
}
