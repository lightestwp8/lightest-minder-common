package gov.tubitak.eu.datagram.DatagramParser;

import java.util.Objects;

public class DNSQuery {
  private final String name;
  private final short qtype;
  private final short qclass;

  public DNSQuery(String name, short qtype, short qclass) {
    this.name = name;
    this.qtype = qtype;
    this.qclass = qclass;
  }


  public String getName() {
    return name;
  }

  public short getQtype() {
    return qtype;
  }

  public short getQclass() {
    return qclass;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (!(o instanceof DNSQuery)) return false;
    DNSQuery dnsQuery = (DNSQuery) o;
    return getQtype() == dnsQuery.getQtype() &&
        getQclass() == dnsQuery.getQclass() &&
        Objects.equals(getName(), dnsQuery.getName());
  }

  @Override
  public int hashCode() {

    return Objects.hash(getName(), getQtype(), getQclass());
  }

  @Override
  public String toString() {
    return "DNSQuery{" +
        "name='" + name + '\'' +
        ", qtype=" + qtype +
        ", qclass=" + qclass +
        '}';
  }
}
