package gov.tubitak.eu.datagram.DatagramParser;

import java.util.Arrays;
import java.util.Objects;

public class DNSResourceRecord {

  private final String name;
  private final short rtype;
  private final short rclass;
  private final int ttl;
  private final byte[] rdata;

  private final static String HEX = "0123456789abcdef";

  final protected static char[] hexArray = {'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};

  private  String bytesToHex(byte[] bytes) {
    char[] hexChars = new char[bytes.length * 2];
    int v;
    for ( int j = 0; j < bytes.length; j++ ) {
      v = bytes[j] & 0xFF;
      hexChars[j * 2] = hexArray[v >>> 4];
      hexChars[j * 2 + 1] = hexArray[v & 0x0F];
    }
    return new String(hexChars);
  }

  public DNSResourceRecord(String name, short rtype, short rclass, int ttl, byte[] rdata) {
    this.name = name;
    this.rtype = rtype;
    this.rclass = rclass;
    this.ttl = ttl;
    this.rdata = rdata;
  }

  public String getName() {
    return name;
  }

  public short getRtype() {
    return rtype;
  }

  public short getRclass() {
    return rclass;
  }

  public int getTtl() {
    return ttl;
  }

  public byte[] getRdata() {
    return rdata;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (!(o instanceof DNSResourceRecord)) return false;
    DNSResourceRecord that = (DNSResourceRecord) o;
    return getRtype() == that.getRtype() &&
        getRclass() == that.getRclass() &&
        getTtl() == that.getTtl() &&
        Objects.equals(getName(), that.getName()) &&
        Arrays.equals(getRdata(), that.getRdata());
  }

  @Override
  public int hashCode() {

    int result = Objects.hash(getName(), getRtype(), getRclass(), getTtl());
    result = 31 * result + Arrays.hashCode(getRdata());
    return result;
  }

  @Override
  public String toString() {
    final StringBuilder builder = new StringBuilder();
    builder.append("ResourceRecord { name: ");
    builder.append(name);
    builder.append(", rtype: ");
    builder.append(rtype);
    builder.append(", rclass: ");
    builder.append(rclass);
    builder.append(", ttl: ");
    builder.append(ttl);
    builder.append(", rdata: ");
    //for (int i = 0; i < rdata.length; ++i) {
      //final byte b = rdata[i];
      //builder.append(HEX.charAt((b & 0xff) >> 4));
      //builder.append(HEX.charAt((b & 0x0f)));
    //}

    String hexStr = bytesToHex(rdata);
    for (int i = 0; i < hexStr.length(); i+=2) {
      String str = hexStr.substring(i, i+2);
      builder.append((char)Integer.parseInt(str, 16));
    }


    //builder.append(bytesToHex(rdata));
    builder.append(" }");
    return builder.toString();
  }
}
