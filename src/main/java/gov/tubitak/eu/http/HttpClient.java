package gov.tubitak.eu.http;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.channels.WritableByteChannel;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.util.List;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

/**
 * Simple utilities for GET and POST
 *
 * @author edona, myildiz
 */
public class HttpClient {

  static {
    /**
     * No need to chit chat
     */
    disableSslVerification();
  }

  public static void httpGET(URL url) {
    try {
      HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

      if (urlConnection.getResponseCode() != HttpURLConnection.HTTP_OK) {
        throw new IllegalStateException("HTTP Request failed with error code " + urlConnection.getResponseCode());
      }
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  /**
   * Download the file linked to the URL
   */
  public static byte[] downloadFile(URL url) {
    try {
      ReadableByteChannel inchannel = Channels.newChannel(url.openStream());
      ByteArrayOutputStream baos = new ByteArrayOutputStream();
      WritableByteChannel outChannel = Channels.newChannel(baos);
      fastChannelCopy(inchannel, outChannel);
      inchannel.close();
      outChannel.close();
      return baos.toByteArray();
    } catch (Exception ex) {
      if (ex instanceof RuntimeException) {
        throw (RuntimeException) ex;
      }
      throw new RuntimeException(ex);
    }
  }

  /**
   * post the given Key-Value pairs to the <code>url</code>
   */
  public static void httpPOSTKVP(URL url, List<byte[]> data) {
    try {

      //combine all the input, with 4 byte prefix lengths
      ByteArrayOutputStream baos = new ByteArrayOutputStream();
      DataOutputStream dataOutputStream = new DataOutputStream(baos);

      //TODO: write ASN.1 in the future
      if (data != null && data.size() != 0) {

        //write how many items we have
        dataOutputStream.writeShort(data.size());

        for (byte[] entry : data) {
          dataOutputStream.writeShort(entry.length);
          dataOutputStream.write(entry);

        }
      }

      byte[] postData = baos.toByteArray();

      HttpURLConnection conn = (HttpURLConnection) url.openConnection();
      conn.setDoOutput(true);
      conn.setInstanceFollowRedirects(false);
      conn.setRequestMethod("POST");
      conn.setRequestProperty("Content-Type", "application/octet-stream");
      conn.setRequestProperty("Content-Length", Integer.toString(postData.length));
      conn.setUseCaches(false);
      try (DataOutputStream wr = new DataOutputStream(conn.getOutputStream())) {
        wr.write(postData);
        wr.flush();
      }

      if (conn.getResponseCode() != HttpURLConnection.HTTP_OK) {
        throw new IllegalStateException("HTTP Request failed with error code " + conn.getResponseCode());
      }
    } catch (MalformedURLException ex) {
      throw new IllegalStateException(ex);
    } catch (ProtocolException ex) {
      throw new IllegalStateException(ex);
    } catch (IOException ex) {
      throw new IllegalStateException(ex);
    }
  }


  /**
   * Code taken from <a href="https://thomaswabner.wordpress.com/2007/10/09/fast-stream-copy-using-javanio-channels/">
   * https://thomaswabner.wordpress.com/2007/10/09/fast-stream-copy-using-javanio-channels/</a>
   *
   * @param src source channel
   * @param dest destination channel
   * @throws IOException if a problem occurs during the transfer
   */
  public static void fastChannelCopy(final ReadableByteChannel src, final WritableByteChannel dest) throws IOException {
    final ByteBuffer buffer = ByteBuffer.allocateDirect(16 * 1024);
    while (src.read(buffer) != -1) {
      // prepare the buffer to be drained
      buffer.flip();
      // write to the channel, may block
      dest.write(buffer);
      // If partial transfer, shift remainder down
      // If buffer is empty, same as doing clear()
      buffer.compact();
    }
    // EOF will leave buffer in fill state
    buffer.flip();
    // make sure the buffer is fully drained.
    while (buffer.hasRemaining()) {
      dest.write(buffer);
    }
  }

  static void disableSslVerification() {
    try {
      // Create a trust manager that does not validate certificate chains
      TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
          return null;
        }

        public void checkClientTrusted(X509Certificate[] certs, String authType) {
        }

        public void checkServerTrusted(X509Certificate[] certs, String authType) {
        }
      }
      };

      // Install the all-trusting trust manager
      SSLContext sc = SSLContext.getInstance("SSL");
      sc.init(null, trustAllCerts, new java.security.SecureRandom());
      HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

      // Create all-trusting host name verifier
      HostnameVerifier allHostsValid = new HostnameVerifier() {
        public boolean verify(String hostname, SSLSession session) {
          return true;
        }
      };

      // Install the all-trusting host verifier
      HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
    } catch (NoSuchAlgorithmException e) {
      e.printStackTrace();
    } catch (KeyManagementException e) {
      e.printStackTrace();
    }
  }
}
