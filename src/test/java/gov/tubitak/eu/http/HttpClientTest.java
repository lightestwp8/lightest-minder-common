package gov.tubitak.eu.http;

import java.util.AbstractMap.SimpleEntry;
import java.util.Arrays;
import org.junit.Ignore;
import org.junit.Test;

import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import static org.junit.Assert.*;

/**
 * @author: myildiz
 * @date: 26.02.2018.
 */
public class HttpClientTest {

  @Ignore
  @Test
  public void httpGET() throws MalformedURLException {
    HttpClient.httpGET(new URL("http://www.google.com/"));

    try {

      HttpClient.httpGET(new URL("http://www.aaaasjhdfkajshdjfahlskjdfh/asdfasdf"));

      throw new RuntimeException("nonexistent url must fail");
    } catch (Exception ex) {
      System.out.println(ex.getMessage());
      //success
    }
  }

  @Ignore
  @Test
  public void downloadFile() throws IOException {
    byte[] bytes = HttpClient.downloadFile(new URL("https://www.tccb.gov.tr/assets/dosya/kurumsalkimlik.pdf"));
    FileOutputStream fos = new FileOutputStream("pdf.pdf");
    fos.write(bytes);
    fos.close();
  }

  @Ignore
  @Test
  public void httpPOSTKVP() throws MalformedURLException {


    HttpClient.httpPOSTKVP(new URL("http://localhost:9502/triggerTest"), Arrays.asList(

        new byte[700],
        new byte[1000]


    ));
  }

  @Ignore
  @Test
  public void fastChannelCopy() {
  }
}