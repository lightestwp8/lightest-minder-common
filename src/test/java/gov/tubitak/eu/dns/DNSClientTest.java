package gov.tubitak.eu.dns;

import org.junit.Assert;
import org.junit.Test;
import gov.tubitak.eu.dns.DNSClient;
import org.xbill.DNS.*;

import java.net.UnknownHostException;

public class DNSClientTest {

  public boolean minderMode = false ;

@Test
  public void testIssueDNSQuery() throws TextParseException, UnknownHostException {

  final DNSClient dnsClient= new DNSClient();

  String lhost = "localhost";
  int port;

  String host = "mindertestbed.org";
  String query1 = "_scheme._trust.a-trust.al";
  int type = Type.PTR;
  int dclass = DClass.IN;

  if(minderMode) {
    port = 9988;
    host = lhost;

  } else {
    port=53;
  }

  String[] rrResponse1 = dnsClient.issueDNSQuery(host,port, query1,type,dclass,false);
  for(String rrName : rrResponse1){
    System.out.println(rrName);

  }
/*
  String query2 = "_scheme._trust.a-trust-repo.al";
  type = Type.URI;
  dclass = DClass.IN;

  String[] rrResponse2 = dnsClient.issueDNSQuery(host,port,query2,type,dclass,false);
  for(String rrName : rrResponse2){
    System.out.println(rrName);

  }


  Assert.assertFalse(rrResponse1.length==0 || rrResponse2.length==0);
*/

}

}
